const getNext = (array, element) => {
  const index = array.indexOf(element)
  return array[index + 1] || array[0]
}

const getPrev = (array, element) => {
  const index = array.indexOf(element)
  return array[index - 1] || array[array.length - 1]
}

export {getNext, getPrev}