import {ConvertApiResponse} from '../model/ConvertApiResponse';

const API_PATH = 'https://openexchangerates.org/api';
const API_KEY = 'app_id=272ba1e3b5b04809aba3d628be2ada0c';

const TRANSACTIONS_API = 'http://private-edf52ff-awesomebank.apiary-mock.com/v1/transactions/'
export function getCurrentRates(): Promise<ConvertApiResponse> {
  return fetch(`${API_PATH}/latest.json?${API_KEY}`).then(response => response.json())
}

export function getInitialTransactions() {
  return fetch(`${TRANSACTIONS_API}/from/2020-01-01-11-00-00/to/2020-01-01-12-00-00`, {
    method: 'POST',
        body: JSON.stringify({})
  }).then(response => response.json())
}

export function getTransactions() {
  return fetch(`${TRANSACTIONS_API}/from/2020-01-01-12-00-00/to/2020-01-01-13-00-00`)
      .then(response => response.json())
}