import React from 'react';
import renderer from 'react-test-renderer';
import ExchangeScreen from '../components/ExchangeScreen/ExchangeScreen';
import {Currency} from '../enums/Currency';

it('ExchangeScreen renders CurrencySelectors', () => {
  const tree = renderer
      .create(<ExchangeScreen accounts={[]}
                              baseAccount={{balance: 0, currency: Currency.EUR}}
                              targetAccount={{balance: 0, currency: Currency.PLN}}
                              onExchanged={jest.fn()} onCancel={jest.fn()}/>)
      .toJSON();
  expect(tree).toMatchSnapshot();
});