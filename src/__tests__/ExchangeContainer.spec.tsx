import React from 'react';
import renderer from 'react-test-renderer';
import ExchangeContainer from '../components/ExchangeContainer/ExchangeContainer';

it('ExchangeContainer renders', () => {
  const tree = renderer
      .create(<ExchangeContainer />)
      .toJSON();
  expect(tree).toMatchSnapshot();
});