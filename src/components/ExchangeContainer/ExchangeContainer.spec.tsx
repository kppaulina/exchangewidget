import * as React from 'react';
import {act} from 'react-dom/test-utils';
import {render, unmountComponentAtNode} from 'react-dom';
import ExchangeContainer from './ExchangeContainer';

describe('ExchangeContainer', function () {

  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  it('should display accounts summary on init', function () {
    const container = document.createElement('div');
    document.body.appendChild(container);
    act(() => {
      render(<ExchangeContainer/>, container);
    })

    const accountSummary = container.querySelector('AccountSummary');
    expect(accountSummary).toBeDefined()
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });
});
