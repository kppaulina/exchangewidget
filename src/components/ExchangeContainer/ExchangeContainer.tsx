import React, {Component} from 'react';
import {ExchangeContainerState} from '../../model/ExchangeContainerState';
import {Currency} from '../../enums/Currency';
import ExchangeScreen from '../ExchangeScreen/ExchangeScreen';
import AccountsSummary from '../AccountsSummary/AccountsSummary';
import {Account} from '../../model/Account';
import TransactionList from "../TransactionList/TransactionList";

export default class ExchangeContainer extends Component<{}, ExchangeContainerState> {

  defaultBalance = 200
  defaultAccount = {
    currency: Currency.EUR,
    balance: this.defaultBalance
  }

  constructor(props: {}) {
    super(props);
    this.state = {
      accounts: [
        this.defaultAccount,
        {
          currency: Currency.GBP,
          balance: this.defaultBalance
        }, {
          currency: Currency.USD,
          balance: this.defaultBalance
        }, {
          currency: Currency.PLN,
          balance: this.defaultBalance
        }],
      exchange: false,
      activeAccount: this.defaultAccount
    }
  }

  onExchangeClicked = (activeAccount: Account): void => {
    this.setState({exchange: true, activeAccount})
  }

  updateBalances = (baseCurrency: Currency, targetCurrency: Currency, baseAmount: string, targetAmount: string): void => {
    const accounts = [...this.state.accounts]
    accounts.map(account => {
      if (account.currency === baseCurrency) {
        account.balance -= parseFloat(baseAmount)
      }
      if (account.currency === targetCurrency) {
        account.balance += parseFloat(targetAmount)
      }
      return account
    })
    const targetAccount = accounts.find(a => a.currency === targetCurrency)
    this.setState({accounts, exchange: false, activeAccount: targetAccount})
  }

  onCancel = (): void => this.setState({exchange: false})

  render() {
    return (<div
        className="font-light bg-blue-600 w-1/2 rounded-lg flex justify-center flex-col items-center m-auto text-gray-100 py-12">
      {this.state.exchange
          ? <ExchangeScreen accounts={this.state.accounts}
                            baseAccount={this.state.activeAccount}
                            targetAccount={this.state.activeAccount.currency === Currency.EUR ? this.state.accounts[1] : this.state.accounts[0]}
                            onExchanged={(baseCurrency, targetCurrency, baseAmount, targetAmount) => this.updateBalances(baseCurrency, targetCurrency, baseAmount, targetAmount)}
                            onCancel={this.onCancel}/>
          : <AccountsSummary
              accounts={this.state.accounts}
              onExchangeClicked={(activeAccount) => this.onExchangeClicked(activeAccount)}/>
      }
      <TransactionList/>
    </div>)
  }
}