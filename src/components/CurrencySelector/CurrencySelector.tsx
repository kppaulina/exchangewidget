import React from 'react';
import {CurrencySelectorProps} from '../../model/CurrencySelectorProps';
import {CurrencySymbol} from '../../enums/Currency';
import PreviousIcon from '../PreviousIcon/PreviousIcon';
import CurrencyInput from 'react-currency-input-field';
import NextIcon from '../NextIcon/NextIcon';

export default function CurrencySelector(props: CurrencySelectorProps) {

  return (
      <form className="text-5xl pb-12 flex items-center">
        <PreviousIcon onIconClick={() => props.onPrevAccount(props.type)}/>
        <div className="flex flex-col mt-8">
          <div className="flex justify-between">
            <label htmlFor="amount" className="mt-1 p-4">{props.account.currency}</label>
            <CurrencyInput className="bg-transparent mt-1 w-2/4 text-right p-4 font-light"
                           prefix={props.type === 'base' && props.amount !== '0' ? '- ' : ''}
                           value={props.amount}
                           onValueChange={(value) => props.onAmountChanged(value, props.type)}
                           allowNegativeValue={false}/>
          </div>
          <div className="text-xl flex justify-between">
            <div>{'You have ' + CurrencySymbol[props.account.currency] + props.account.balance}</div>
          </div>
        </div>
        <NextIcon onIconClick={() => props.onNextAccount(props.type)}/>
      </form>
  )
}