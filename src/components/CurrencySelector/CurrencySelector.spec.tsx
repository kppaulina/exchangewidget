import * as React from 'react';
import {act} from 'react-dom/test-utils';
import {render, unmountComponentAtNode} from 'react-dom';
import CurrencySelector from './CurrencySelector';
import {Currency} from '../../enums/Currency';

describe('CurrencySelector', function () {
  
  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });
  
  it('should display icons and input', function () {
    const container = document.createElement('div');
    document.body.appendChild(container);
    act(() => {
      render(<CurrencySelector onNextAccount={jest.fn()} onPrevAccount={jest.fn()} account={{balance: 0, currency: Currency.EUR}} type={'base'} amount={'10'} onAmountChanged={jest.fn()}/>, container);
    })

    const prevIcon = container.querySelector('PreviousIcon');
    const nextIcon = container.querySelector('NextIcon');
    const input = container.querySelector('CurrencyInput');
    expect(prevIcon).toBeDefined()
    expect(nextIcon).toBeDefined()
    expect(input).toBeDefined()
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });
});
