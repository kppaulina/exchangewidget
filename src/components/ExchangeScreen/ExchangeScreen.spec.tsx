import * as React from 'react';
import {act} from 'react-dom/test-utils';
import {render, unmountComponentAtNode} from 'react-dom';
import ExchangeScreen from "./ExchangeScreen";
import {Currency} from "../../enums/Currency";

describe('ExchangeScreen', function () {
  
  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  it('should display error message when request failed', function () {
    const container = document.createElement('div');
    document.body.appendChild(container);

    global.fetch = jest.fn().mockImplementation(() => Promise.resolve({
      json: () => Promise.reject()
    }));

    act(() => {
      render(<ExchangeScreen accounts={[]} baseAccount={{balance: 0, currency: Currency.EUR}} targetAccount={{balance: 0, currency: Currency.PLN}} onExchanged={jest.fn()} onCancel={jest.fn()}/>, container);
    })

    const errorMessage = container.querySelector('#refreshButton');
    expect(errorMessage).toBeDefined()
  });

  it('should not display error message on init', function () {
    const container = document.createElement('div');
    document.body.appendChild(container);

    act(() => {
      render(<ExchangeScreen accounts={[]} baseAccount={{balance: 0, currency: Currency.EUR}} targetAccount={{balance: 0, currency: Currency.PLN}} onExchanged={jest.fn()} onCancel={jest.fn()}/>, container);
    })

    const errorMessage = container.querySelector('#refreshButton');
    expect(errorMessage).toEqual(null)
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });
});
