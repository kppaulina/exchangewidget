import React, {Component} from 'react';
import {getCurrentRates} from '../../services/ExchangeService';
import CurrencySelector from '../CurrencySelector/CurrencySelector';
import {ExchangeScreenState} from '../../model/ExchangeScreenState';
import {ExchangeScreenProps} from '../../model/ExchangeScreenProps';
import {AccountType} from '../../model/CurrencySelectorProps';
import ExchangeButton from '../ExchangeButton/ExchangeButton';
import {getNext, getPrev} from '../../utils/arrayUtils';
import {CurrencySymbol} from "../../enums/Currency";

export default class ExchangeScreen extends Component<ExchangeScreenProps, ExchangeScreenState> {

  fetchRatesInterval

  constructor(props: ExchangeScreenProps) {
    super(props);
    this.state = {
      base: {
        account: this.props.baseAccount,
        amount: '0'
      },
      target: {
        account: this.props.targetAccount,
        amount: '0'
      },
      hasError: false,
      exceedBalance: false,
      rates: undefined
    }
  }

  componentDidMount(): void {
    this.fetchData();
    this.fetchRatesInterval = setInterval(this.fetchData, 10000);
  }

  componentWillUnmount(): void {
    clearInterval(this.fetchRatesInterval);
  }

  fetchData = async () => {
    try {
      const response = await getCurrentRates();
      this.setState({
        rates: response.rates,
        hasError: false
      }, () => this.onSelectedAmount(this.state.base.amount, 'base'))
    } catch (e) {
      this.setState({hasError: true})
    }
  }

  getAmount = (baseCurrency: string, amount: string, targetCurrency: string): string => {
    const base = parseFloat(amount) / this.state.rates[baseCurrency]
    return (this.state.rates[targetCurrency] * base).toFixed(2)
  }

  onSelectedAmount = (amount = '0', type: AccountType): void => {
    const secondType = type === 'base' ? 'target' : 'base'
    const exchanged = this.getAmount(this.state[type].account.currency, amount, this.state[secondType].account.currency)
    const state = {
      exceedBalance: parseFloat(exchanged) > this.state[secondType].account.balance
          || parseFloat(amount) > this.state[type].account.balance
    }
    state[type] = {
      ...this.state[type],
      amount
    }
    state[secondType] = {
      ...this.state[secondType],
      amount: exchanged,
      rates: this.state.rates
    }
    this.setState(state)
  }

  exchange = (): void => {
    this.props.onExchanged(this.state.base.account.currency, this.state.target.account.currency,
        this.state.base.amount, this.state.target.amount)
  }

  onPrevAccount = (accountType: AccountType): void => {
    const newState = {}
    newState[accountType] = {
      ...this.state[accountType],
      account: getPrev(this.props.accounts, this.state[accountType].account)
    }
    this.setState(newState, () => this.onSelectedAmount(this.state[accountType].amount, accountType))
  }

  onNextAccount = (accountType: AccountType): void => {
    const newState = {}
    newState[accountType] = {
      ...this.state[accountType],
      account: getNext(this.props.accounts, this.state[accountType].account)
    }
    this.setState(newState, () => this.onSelectedAmount(this.state[accountType].amount, accountType))
  }

  getCurrentRate = (): string => {
    const baseCurrency = this.state.base.account.currency
    const targetCurrency = this.state.target.account.currency
    const baseSymbol = CurrencySymbol[baseCurrency]
    const targetSymbol = CurrencySymbol[targetCurrency]
    return `${baseSymbol}1 = ${targetSymbol}${this.getAmount(baseCurrency, '1', targetCurrency)}`
  }

  render() {
    if (!this.state.hasError || this.state.rates) {
      return (
          <div className="flex flex-col items-center">
            {this.state.rates && <div className="text-ml">{this.getCurrentRate()}</div>}
            <CurrencySelector onAmountChanged={this.onSelectedAmount}
                              amount={this.state.base.amount}
                              account={this.state.base.account}
                              type={'base'}
                              onNextAccount={this.onPrevAccount}
                              onPrevAccount={this.onNextAccount}/>
            <CurrencySelector onAmountChanged={this.onSelectedAmount}
                              amount={this.state.target.amount}
                              account={this.state.target.account}
                              type={'target'}
                              onNextAccount={this.onPrevAccount}
                              onPrevAccount={this.onNextAccount}/>
            <div className="w-3/4 flex justify-between items-end">
              <button className="bg-transparent hover:bg-blue-500 w-36 text-gray-100 p-4 rounded text-2xl font-light"
                      onClick={this.props.onCancel}>Cancel
              </button>
              {this.state.exceedBalance && <div className="text-red-800 mb-4">Exceed balance</div>}
              <ExchangeButton
                  disabled={this.state.target.account.currency === this.state.base.account.currency || this.state.exceedBalance}
                  onButtonClicked={this.exchange}/>
            </div>
          </div>
      )
    } else {
      return <div className="text-ml">
        There was an error while fetching rates.
        <button id="refreshButton" role="button"
                className="mt-5 m-auto block bg-transparent bg-gray-100 hover:bg-gray-200 w-36 text-blue-600 py-2 px-4 rounded"
                onClick={this.fetchData}>Refresh</button>
      </div>
    }
  }
}