import React from 'react';
import {ExchangeButtonProps} from '../../model/ExchangeButtonProps';

export default function ExchangeButton(props: ExchangeButtonProps) {
  return (
      <button className="disabled:opacity-50 bg-gray-100 hover:bg-gray-200 w-36 text-blue-600 p-4 rounded text-2xl font-light"
              disabled={props.disabled}
              onClick={props.onButtonClicked}>
        Exchange
      </button>
  )
}