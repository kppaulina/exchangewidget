import * as React from 'react';
import {act} from 'react-dom/test-utils';
import {render, unmountComponentAtNode} from 'react-dom';
import AccountsSummary from './AccountsSummary';
import {Currency} from "../../enums/Currency";

describe('AccountsSummary', function () {
  
  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });
  
  it('should display icons and exchange button', function () {
    const container = document.createElement('div');
    document.body.appendChild(container);
    act(() => {
     render(<AccountsSummary accounts={[{balance: 0, currency: Currency.EUR}]} onExchangeClicked={jest.fn()}/>, container);
    })

    const prevIcon = container.querySelector('PreviousIcon');
    const nextIcon = container.querySelector('NextIcon');
    const button = container.querySelector('ExchangeButton');
    expect(prevIcon).toBeDefined()
    expect(nextIcon).toBeDefined()
    expect(button).toBeDefined()
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });
});
