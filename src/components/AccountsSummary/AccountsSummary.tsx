import React, {Component} from 'react';
import {AccountsSummaryProps} from '../../model/AccountsSummaryProps';
import {AccountsSummaryState} from '../../model/AccountsSummaryState';
import ExchangeButton from '../ExchangeButton/ExchangeButton';
import PreviousIcon from '../PreviousIcon/PreviousIcon';
import NextIcon from '../NextIcon/NextIcon';
import {getNext, getPrev} from '../../utils/arrayUtils';

export default class AccountsSummary extends Component<AccountsSummaryProps, AccountsSummaryState> {

  constructor(props: AccountsSummaryProps) {
    super(props);
    this.state = {currentAccount: this.props.accounts[0]}
  }

  onNextAccount = (): void => {
    this.setState({currentAccount: getPrev(this.props.accounts, this.state.currentAccount)})
  }

  onPrevAccount = (): void => {
    this.setState({currentAccount: getNext(this.props.accounts, this.state.currentAccount)})
  }

  render() {
    return (
        <div className='flex items-center py-24'>
          <PreviousIcon onIconClick={this.onPrevAccount}/>
          <div className='px-12 text-center'>
            <div className='block text-5xl font-light mb-8 w-80'>
              <div className='w-1/2 m-8 inline'>{this.state.currentAccount.currency}</div>
              <div className='w-1/2 m-8 inline'>{this.state.currentAccount.balance}</div>
            </div>
            <ExchangeButton onButtonClicked={() => this.props.onExchangeClicked(this.state.currentAccount)}/>
          </div>
          <NextIcon onIconClick={this.onNextAccount}/>
        </div>
    )
  }
}