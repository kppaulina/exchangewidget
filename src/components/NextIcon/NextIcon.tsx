import React from 'react';
import {IconProps} from '../../model/IconProps';

export default function NextIcon(props: IconProps) {
  return (
      <svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" fill="currentColor"
           className="bi bi-chevron-right cursor-pointer text-gray-100" viewBox="0 0 16 16"
           onClick={() => props.onIconClick()}>
        <path fillRule="evenodd"
              d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
      </svg>
  )
}