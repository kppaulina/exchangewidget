import * as React from 'react';
import {act} from 'react-dom/test-utils';
import {render, unmountComponentAtNode} from 'react-dom';
import TransactionList from "./TransactionList";

describe('TransactionList', function () {
  
  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  it('should display initial transactions', function () {
    const container = document.createElement('div');
    document.body.appendChild(container);

    global.fetch = jest.fn().mockImplementation(() => Promise.resolve({
      json: () => Promise.resolve([{
        id: "fdcade3b-f0f4-4dc4-b000-8972eb17e074", currency: "EUR", amount: 5.05
      }, {
        id: "fdcade3b-s0f4-4dc4-b000-8972eb17e074", currency: "PLN", amount: 5.05
      }])
    }));

    act(() => {
      render(<TransactionList/>, container);
    })

    const list = container.querySelector('#transactionsList');
    expect(list).toBeDefined()
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });
});
