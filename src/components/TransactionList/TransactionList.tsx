import React, {Component} from "react";
import {getInitialTransactions, getTransactions} from "../../services/ExchangeService";

export default class TransactionList extends Component<{}, any> {

  constructor(props: {}) {
    super(props)
    this.state = {
      transactions: []
    }
  }

  async componentDidMount() {
    const transactions = await getInitialTransactions()
    this.setState({transactions})
  }

  async refreshTransactions() {
    const newTransactions = await getTransactions()
    this.setState({transactions: [...this.state.transactions, ...newTransactions]})
  }

  render() {
    return (<div>
      <ul id="transactionsList">
      {this.state.transactions.map((transaction) => {
        return <li key={transaction.id} id={transaction.id}>
          <span>{transaction.currency}</span>
          <span>{transaction.amount}</span>
        </li>
      })}
      </ul>
      <button onClick={() => this.refreshTransactions()}>Refresh</button>
    </div>)
  }
}