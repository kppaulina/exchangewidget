export enum Currency {
  PLN = 'PLN',
  EUR = 'EUR',
  GBP = 'GBP',
  USD = 'USD'
}

export enum CurrencySymbol {
  PLN = 'zł',
  EUR = '€',
  GBP = '£',
  USD = '$'
}