import React from 'react';
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary';
import ExchangeContainer from './components/ExchangeContainer/ExchangeContainer';

const App = () => <ErrorBoundary><ExchangeContainer/></ErrorBoundary>;

export default App;
