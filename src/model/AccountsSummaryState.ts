import {Account} from './Account';

export interface AccountsSummaryState {
  currentAccount: Account
}