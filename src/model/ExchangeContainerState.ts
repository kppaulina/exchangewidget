import {Account} from './Account';

export interface ExchangeContainerState {
  accounts: Account[],
  exchange: boolean,
  activeAccount: Account
}