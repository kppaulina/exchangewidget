import {Account} from './Account';

export interface CurrencySelectorProps {
  onAmountChanged: (value, type) => void;
  amount: string;
  account: Account;
  type: AccountType;
  onNextAccount: (type) => void;
  onPrevAccount: (type) => void;
}

export type AccountType = 'base' | 'target'