export interface ExchangeButtonProps {
  onButtonClicked: () => void
  disabled?: boolean
}
