import {Currency} from '../enums/Currency';

export interface Account {
  balance: number,
  currency: Currency
}