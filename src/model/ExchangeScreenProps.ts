import {Account} from "./Account";

export interface ExchangeScreenProps {
  accounts: Account[]
  baseAccount: Account
  targetAccount: Account
  onExchanged: (baseCurrency, targetCurrency, baseAmount, targetAmount) => void
  onCancel: () => void
}