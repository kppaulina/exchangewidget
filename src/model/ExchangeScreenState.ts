import {Account} from "./Account";

export interface ExchangeScreenState {
  target: {
    account: Account;
    amount: string
  };
  base: {
    account: Account;
    amount: string
  };
  exceedBalance: boolean;
  hasError: boolean;
  rates?: {
    PLN: number;
    USD: number;
    EUR: number;
    GBP: number;
  }
}