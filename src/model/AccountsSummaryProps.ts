import {Account} from './Account';

export interface AccountsSummaryProps {
  accounts: Account[],
  onExchangeClicked: (account: Account) => void
}