export interface ConvertApiResponse {
  rates: {
    PLN: number;
    USD: number;
    EUR: number;
    GBP: number;
  }
}