# Exchange Widget

Small app displaying list of accounts with balances with an ability to exchange money between them.

## Technical notes
App is implemented using React 17 using Node in version 14, tested with Jest. 
Styles are built with tailwind.

## Run instruction
Requirements:
- node v14

Run instructions (from root directory)
```
npm install
npm start
```
App is available on http://localhost:8080

## Potential improvements
There are a few areas worth improving in the future:

* updates to exchange logic

* additional unit tests, e2e tests

* extracting API token

* more personalised styles, updates to mobile view